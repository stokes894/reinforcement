#Crazy experiment
#Just keep generating random policies and keep the best one
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

size = 3
numStates = 9
numActionsVec = 4*np.ones(numStates)

Best = RandPolicy(numStates, numActionsVec)
scores = []
iters = 1000
for i in np.arange(iters):
    scores.append(snakeGame(size,Best))
BestVal = []
BestVal.append(np.mean(scores))
#Run a simulation to find the average score under this policy
for j in np.arange(100):
    print(j)
    policy = RandPolicy(numStates, numActionsVec)
    scores = []
    for i in np.arange(iters):
        scores.append(snakeGame(size,policy))
    BestVal.append(np.max((BestVal[j], np.mean(scores))))
    if BestVal[j+1] == np.mean(scores):
        Best = policy
        

plt.plot(BestVal)

#Make a heatmap of the best policy
sns.heatmap(Best,cmap = "RdYlGn")
#Chance to go in each direction
#up
sns.heatmap(Best[:,0].reshape(3,3),cmap = "RdYlGn")
#left
sns.heatmap(Best[:,1].reshape(3,3),cmap = "RdYlGn")
#down
sns.heatmap(Best[:,2].reshape(3,3),cmap = "RdYlGn")
#right
sns.heatmap(Best[:,3].reshape(3,3),cmap = "RdYlGn")

