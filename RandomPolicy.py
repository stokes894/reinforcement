# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 15:49:40 2020

@author: stoke
"""
#To be used with Reinforcement.py to generate a random policy for a set os states and actions
#Produces an array containing the policy
import numpy as np

numStates = 9
numActionsVec = 4*np.ones(numStates)
def RandPolicy(numStates, numActionsVec):
    pol = np.zeros((numStates,np.max(numActionsVec)))
    for i in np.arange(numStates):
        x = np.random.uniform(0,1,numActionsVec[i])
        x = x/np.sum(x)
        pol[i,:] = x 
    return pol