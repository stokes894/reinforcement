# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 20:37:27 2019

@author: stoke
"""
import numpy as np
from random import sample
from numpy.random import choice
import matplotlib.pyplot as plt


#Practicing Reinforcement Learning Ideas by following Reinforcement Learning: An Introduction

#Chapter 2
#10-armed bandit example from 2.3

def TenArmBandit(epsilon, iters):
    q = np.random.normal(0,1,10)
    act = np.zeros((10,1))
    reward = np.zeros((10,1))
    prob = 0.5*np.ones((10,1))
    cumRew = list()
    for i in np.arange(iters):
        best = np.where(prob == np.max(prob))[0]
        if len(best) != 1:
            best = sample(list(best),1)
        if np.random.uniform(0,1,1) < epsilon:
            best = sample(list(np.arange(10)),1)
        act[best] += 1
        rewardTemp = np.random.normal(q[best], 1, 1)
        reward[best] += rewardTemp
        cumRew.append(sum(reward)/sum(act))
        prob[best] = prob[best] + 1/act[best]*(rewardTemp - prob[best])
        prob[np.where(np.isnan(reward/act))[0]] = 0
    return cumRew

a = np.zeros((1000,1))
b = np.zeros((1000,1))
c = np.zeros((1000,1))
for j in np.arange(500):
    if j % 100 == 0:
        print(j)
    a = a + TenArmBandit(epsilon = 0.01, iters = 1000)
    b = b + TenArmBandit(epsilon = 0.1, iters = 1000)
    c = c + TenArmBandit(epsilon = 0, iters = 1000)
plt.plot(a/500)
plt.plot(b/500)
plt.plot(c/500)

#Consider a non-stationary process for the true values
def NSTenArmBandit(alpha, epsilon, iters):
    q = np.zeros((10,1))
    act = np.zeros((10,1))
    reward = np.zeros((10,1))
    prob = 0.5*np.ones((10,1))
    cumRew = list()
    for i in np.arange(iters):
        best = np.where(prob == np.max(prob))[0]
        if len(best) != 1:
            best = sample(list(best),1)
        if np.random.uniform(0,1,1) < epsilon:
            best = sample(list(np.arange(10)),1)
        act[best] += 1
        rewardTemp = np.random.normal(q[best], 1, 1)
        reward[best] += rewardTemp
        cumRew.append(sum(reward)/sum(act))
        if alpha != 0:
            prob[best] = prob[best] + alpha*(rewardTemp - prob[best])
        else:
            prob[best] = prob[best] + 1/act[best]*(rewardTemp - prob[best])
        prob[np.where(np.isnan(reward/act))[0]] = 0
        q += np.random.normal(0,0.01,10).reshape(10,1)
    return cumRew

a = np.zeros((2000,1))
b = np.zeros((2000,1))
for j in np.arange(500):
    if j % 100 == 0:
        print(j)
    a = a + NSTenArmBandit(alpha = 0, epsilon = 0.1, iters = 2000)
    b = b + NSTenArmBandit(alpha=0.1, epsilon = 0.1, iters = 2000)
plt.plot(a/500)
plt.plot(b/500)

#Could instead choose exploration action by measuring distance to optimality (upper confidence bound)
#Difficult to use when many states are present and for non-stationary processes
def UCBTenArmBandit(iters, c):
    q = np.random.normal(0,1,10)
    act = np.zeros((10,1))
    reward = np.zeros((10,1))
    prob = 0.5*np.ones((10,1))
    cumRew = list()
    for i in np.arange(iters):
        ucb = prob + c*(np.log((i+1))/act)
        best = np.where(ucb == np.max(ucb))[0]
        if len(best) > 1:
            best = sample(list(best),1)
        if len(best) < 1:
            best = sample(list(np.arange(10)),1)
        act[best] += 1
        rewardTemp = np.random.normal(q[best], 1, 1)
        reward[best] += rewardTemp
        cumRew.append(sum(reward)/sum(act))
        prob[best] = prob[best] + 1/act[best]*(rewardTemp - prob[best])
        prob[np.where(np.isnan(reward/act))[0]] = 0
    return cumRew

a = np.zeros((2000,1))
b = np.zeros((2000,1))
for j in np.arange(500):
    if j % 100 == 0:
        print(j)
    a = a + TenArmBandit(epsilon = 0.1, iters = 2000)
    b = b + UCBTenArmBandit(c = 2, iters = 2000)
plt.plot(a/500)
plt.plot(b/500)

#Could instead explore based on Stoch. Gradient Ascent with approximated gradient
def SGATenArmBandit(iters, alpha):
    q = np.random.normal(0,1,10)
    act = np.zeros((10,1))
    reward = np.zeros((10,1))
    H = np.zeros((10,1))
    cumRew = list()
    for i in np.arange(iters):
        prob = np.exp(H)/sum(np.exp(H))
        best = choice(list(np.arange(10)),1, p = prob.reshape(10,))
        act[best] += 1
        rewardTemp = np.random.normal(q[best], 1, 1)
        reward[best] += rewardTemp
        cumRew.append(sum(reward)/sum(act))
        for j in np.arange(10):
            if j == best:
               H[j] += alpha*(rewardTemp - np.mean(reward))*(1-prob[j]) 
            else:
               H[j] -= alpha*(rewardTemp - np.mean(reward))*(prob[j]) 
    return cumRew

a = np.zeros((2000,1))
b = np.zeros((2000,1))
for j in np.arange(500):
    if j % 100 == 0:
        print(j)
    a = a + TenArmBandit(epsilon = 0.1, iters = 2000)
    b = b + SGATenArmBandit(alpha = 0.1, iters = 2000)
plt.plot(a/500)
plt.plot(b/500)

#Chapter 3
#Snake problem. Solve for value function. 3x3 grid. 9 states with apple in top-left.
#Set up a system of linear equations using Bellman optimality equation and solve
#9x9 coefficient matrix and 9x1 response vector
a = np.array([(-11/20, 9/40, 0, 9/40, 0,0,0,0,0),(9/40,-31/40,9/40,0,9/40,0,0,0,0),(0, 9/40, -11/20, 0,0,9/40,0,0,0),(9/40,0,0,-31/40,9/40,0,9/40,0,0),(0,9/40,0,9/40,-1,9/40,0,9/40,0),(0,0,9/40,0,9/40,-31/40,0,0,9/40),(0,0,0,9/40,0,0,-11/20,9/40,0),(0,0,0,0,9/40,0,9/40,-31/40,9/40),(0,0,0,0,0,9/40,0,9/40,-11/20)])
b = np.array([(1/2, 1/4, 1/2, 1/4, 0, -9/4, 1/2, -9/4, 1/2)]).reshape(9,)
np.linalg.solve(a,b) #Works well
#Solving the system of equations is computationally intensive

#Chapter 4
#To get the value function for any policy, just iteratively update an initial guess (sweep operator)
def sweep(init, epsilon, gamma, policy):
    delta = 10
    while delta>epsilon:
        initOld = init
        init[0] = policy[0,0]*(-1+gamma*init[0])+policy[0,1]*(-1+gamma*init[0])+policy[0,2]*(gamma*init[1])+policy[0,3]*(gamma*init[3])
        init[2] = policy[0,1]*(-1+gamma*init[2])+policy[0,2]*(-1+gamma*init[2])+policy[0,0]*(gamma*init[1])+policy[0,3]*(gamma*init[5])
        init[6] = policy[0,0]*(-1+gamma*init[6])+policy[0,3]*(-1+gamma*init[6])+policy[0,1]*(gamma*init[3])+policy[0,2]*(gamma*init[7])
        init[8] = policy[0,2]*(-1+gamma*init[8])+policy[0,3]*(-1+gamma*init[8])+policy[0,0]*(gamma*init[7])+policy[0,1]*(gamma*init[5])
        init[1] = policy[0,1]*(-1+gamma*init[1])+policy[0,2]*(gamma*init[2])+policy[0,3]*(gamma*init[4])+policy[0,0]*(gamma*init[0])
        init[3] = policy[0,0]*(-1+gamma*init[3])+policy[0,1]*(gamma*init[0])+policy[0,2]*(gamma*init[4])+policy[0,3]*(gamma*init[6])
        init[5] = policy[0,2]*(-1+gamma*init[5])+policy[0,3]*(10+gamma*init[8])+policy[0,0]*(gamma*init[4])+policy[0,2]*(gamma*init[2])
        init[7] = policy[0,3]*(-1+gamma*init[7])+policy[0,2]*(10+gamma*init[8])+policy[0,0]*(gamma*init[6])+policy[0,1]*(gamma*init[4])
        init[4] = policy[0,0]*(gamma*init[3])+policy[0,1]*(gamma*init[1])+policy[0,2]*(gamma*init[5])+policy[0,3]*(gamma*init[7])
        delta = sum(np.abs(init-initOld))
    return init

init = np.array([10,10,10,10,10,10,10,10,10]); epsilon = 0.00001; gamma = 0.9
policy = np.array([[1,2,3,4],[1,2,4,3],[1,3,2,4],[1,3,4,2],[1,4,2,3],[1,4,3,2],[2,1,3,4],[2,1,4,3],[2,3,1,4]])/10
sweep(init,epsilon, gamma,policy)

init = np.array([10,10,10,10,10,10,10,10,10]); epsilon = 0.00001; gamma = 0.9
policy = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1],[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1],[1,0,0,0]])
sweep(init,epsilon, gamma,policy)

#Replace policy with one by making it greedy wrt its value function 
#def improve(policy, value):
    
#    return newPolicy


