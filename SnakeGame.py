# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 16:02:33 2020

@author: stoke
"""
#To be used with Reinforcement.py to generate snake games according to a policy
import numpy as np
import random
import matplotlib.pyplot as plt

#Generates a new snake game
def getNewGame(size=3):
    board = np.zeros((size*size))
    apple = random.randint(0,np.prod(board.shape)-1)
    snake = apple
    while snake == apple:
        snake = random.randint(0,np.prod(board.shape)-1)
    board[snake] = 1
    board[apple] = 2
    board = board.reshape(size,size)
    return board

#Evaluate a move of the snake
def getReward(board, move, size):
    snake = np.where(board == 1)
    apple = np.where(board == 2)
    moves = getValidMoves(snake,size)
    if moves not in move: 
        return snake,apple,-1
    if move in (0,2):
        snake = tuple((snake[0] + np.sign(move-1), snake[1]))
    if move in (1,3):
        snake = tuple((snake[0], snake[1] + np.sign(move-2)))
    if snake != apple:
        return snake,apple,0
    while apple == snake:
        apple = tuple(([random.randint(0,size-1)],[random.randint(0,size-1)]))
    return snake, apple, 10
    
#update the board based on the snake moves    
def updateBoard(board,snake,apple):
    board = np.zeros(board.shape)
    board[snake] = 1
    board[apple] = 2
    return board

#Given a board and a snake position get the valid moves (up,left,down,right)
def getValidMoves(snake,size):
    keep = list()
    for x in list((0,size-1)):
        keep.append(x not in snake[0])
        keep.append(x not in snake[1])
    moves = np.where(keep)
    return moves

#Runs the game according to some policy and returns the score
#Policy is a state x action sized array (in this case 9x4)
def snakeGame(size, policy):
    board = getNewGame(size = size)
    score = 0; rew = 0
    snake = np.where(board == 1)
    while rew != 10:
        move = np.random.choice(4, 1, p=policy[3*snake[0]+snake[1],][0])
        snake,apple,rew = getReward(board, move, size)
        if rew != -1:
            board = updateBoard(board,snake,apple)
        score = score + rew
    return score

size = 3
numStates = 9
numActionsVec = 4*np.ones(numStates)
policy = RandPolicy(numStates, numActionsVec)

#Run a simulation to find the average score under this policy
scores = []
avgScore = []
iters = 1000
for i in np.arange(iters):
    scores.append(snakeGame(size,policy))
    avgScore.append(np.mean(scores))
plt.plot(scores)
plt.plot(avgScore)
